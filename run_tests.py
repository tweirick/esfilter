import unittest
from esfilterlib.parsers import BED
from esfilterlib.filters import *


from intervaltree import IntervalTree, Interval

# python3 -m unittest run_tests.py


def test_values(expected_value, test_value):
    """

    :param expected_value:
    :param test_value:
    :return:
    """
    test_result = expected_value == test_value

    if not test_result:
        print(expected_value, "!=", test_value)
        print(type(expected_value))
        print(type(test_value))

    return test_result


class ParserTests(unittest.TestCase):
    # ========================================================================
    # Start pythonic_coordinates_to_exact_coordinates() tests
    # (slice_str, row_len, split_char=",", range_char=":")
    # return list - containing coordinates
    # ========================================================================

    def test__is_base_a_snp_test_on_a_snp(self):

        function_output = is_snp("rSomething" )
        expected_output = True
        self.assertTrue(test_values(function_output, expected_output))

    def test__is_base_a_snp_test_on_a_nonsnp(self):
        function_output = is_snp(".")
        expected_output = False
        self.assertTrue(test_values(function_output, expected_output))

    def test__is_nonediting_mismatch_on_T_to_A_nonediting_mismatch(self):
        reference_base = "T"
        alternative_base = "A"
        function_output = is_nonediting_mismatch(reference_base, alternative_base)
        expected_output = True
        self.assertTrue(test_values(function_output, expected_output))

    def test__is_nonediting_mismatch_on_C_to_T_nonediting_mismatch(self):
        reference_base = "C"
        alternative_base = "T"
        function_output = is_nonediting_mismatch(reference_base, alternative_base)
        expected_output = True
        self.assertTrue(test_values(function_output, expected_output))

    def test__is_nonediting_mismatch_on_T_to_C_editing_mismatch(self):
        reference_base = "T"
        alternative_base = "C"
        function_output = is_nonediting_mismatch(reference_base, alternative_base)
        expected_output = False
        self.assertTrue(test_values(function_output, expected_output))

    def test__adding_a_record_to_abed_file(self):

        bed_obj = BED()
        chromosome, start, stop = "1", "100", "200"
        bed_obj.add_interval(chromosome, start, stop)

        test_interval = dict()
        test_interval[chromosome] = IntervalTree()
        test_interval[chromosome][int(start):int(stop)] = True

        expected_output = test_interval
        function_output = bed_obj.interval_tree_bed_dict

        self.assertTrue(test_values(function_output, expected_output))

    def test__query_to_find_if_base_within_region_easy_overlapping_case(self):
        bed_obj = BED()
        chromosome, start, stop = "1", 100, 200
        bed_obj.add_interval(chromosome, start, stop)

        snv_chromosome = "1"
        snv_position = 150

        function_output = bed_obj.is_base_within_region(snv_chromosome, snv_position)
        expected_output = True

        self.assertTrue(test_values(function_output, expected_output))

    def test__query_to_find_if_base_within_region_start_bound_overlap(self):
        bed_obj = BED()
        chromosome, start, stop = "1", 100, 200
        bed_obj.add_interval(chromosome, start, stop)

        snv_chromosome = "1"
        snv_position = 101

        function_output = bed_obj.is_base_within_region(snv_chromosome, snv_position)
        expected_output = True

        self.assertTrue(test_values(function_output, expected_output))

    def test__query_to_find_if_base_within_region_stop_bound_overlap(self):
        bed_obj = BED()
        chromosome, start, stop = "1", 100, 200
        bed_obj.add_interval(chromosome, start, stop)

        snv_chromosome = "1"
        snv_position = 200

        function_output = bed_obj.is_base_within_region(snv_chromosome, snv_position)
        expected_output = True

        self.assertTrue(test_values(function_output, expected_output))

    def test__query_to_find_if_base_within_region_no_overlap(self):
        bed_obj = BED()
        chromosome, start, stop = "1", 100, 200
        bed_obj.add_interval(chromosome, start, stop)

        snv_chromosome = "1"
        snv_position = 900

        function_output = bed_obj.is_base_within_region(snv_chromosome, snv_position)
        expected_output = False

        self.assertTrue(test_values(function_output, expected_output))


if __name__ == "__main__":
    unittest.main()

# test_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# list_length = len(test_list)
# coordinate_str = "0"
# expected_output = [0]
# function_output = utilities.pythonic_coordinates_to_exact_coordinates_list(coordinate_str, list_length)



