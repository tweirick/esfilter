./esfilter.py --test-all \
  --vcf                         test_data/SRR915765.5000.TC.vcf                                \
  --bam                         test_data/SRR915765.noDup.realigned.recalibrated.bam           \
  --reference-genome            test_data/Homo_sapiens.GRCh38.dna.primary_assembly.fa          \
  --splice-junctions            test_data/Homo_sapiens.GRCh38.90.gtf.splice_junctions.bed.9000 \
  --repeat-regions              test_data/e90_human_alu_repeats.bed.9000                       \
  --filter-stats                test_data/SRR915765.filter_stats.tsv                           \
  --repeat-regions-out-vcf      test_data/SRR915765.filtered_repeats.vcf                       \
  --non-repeat-regions-out-vcf  test_data/SRR915765.all.filtered_nonrepeats.vcf                \
  --in-repeat-regions-out-fasta test_data/SRR915765.all.nonrepeats.fasta


