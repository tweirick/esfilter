
def remove_duplicates(psl_file, variant_file, min_base_qual, min_mismatch):

    blat_dict = {}

    for line in open(psl_file):  # Summarize the blat hits

        psl_fields = line.split()

        chromosome, pos, ref, alt, mm_read_count = psl_fields[9].split(":")

        var_tuple = (chromosome, int(pos), ref, alt)

        # #of Matches, targetName, block_count, blockSize, targetStarts
        blat_score = [psl_fields[0], psl_fields[13], psl_fields[17], psl_fields[18], psl_fields[20]]

        if var_tuple in blat_dict:
            blat_dict[var_tuple] = blat_dict[var_tuple] + [blat_score]
        else:
            blat_dict[var_tuple] = [blat_score]

    site_dict = {}
    discard_dict = {}
    # loop over blat Hits

    for varTuple in blat_dict.keys():  # Loop over all blat hits of mmReads to observe the number of Alignements

        keep_snp = False

        chromosome, pos, ref, alt = varTuple
        psl_line = blat_dict[varTuple]
        largest_score = 0
        largest_score_line = psl_line[0]
        score_array = []

        # look for largest blatScore and save the largest line too
        for blatHit in psl_line:
            line_score = int(blatHit[0])
            score_array.append(line_score)
            if line_score > largest_score:
                largest_score = line_score
                largest_score_line = blatHit

        score_array.sort(reverse=True)

        if len(score_array) < 2:  # test if more than one blat Hit exists
            score_array.append(0)

        # Check if same chromosome and hit is lower the 95 percent of first hit.
        if chr == largest_score_line[1] and score_array[1] < score_array[0]*0.95:

            block_count = int(largest_score_line[2])
            block_sizes = largest_score_line[3].split(",")[:-1],
            block_starts = largest_score_line[4].split(",")[:-1]

            for i in range(block_count):
                start_pos = int(block_starts[i])+1
                end_pos = start_pos + int(block_sizes[i])

                # check if alignment overlaps mismatch
                # if pos >= start_pos and pos < end_pos:
                if start_pos <= pos < end_pos:
                    keep_snp = True

        if keep_snp:
            if varTuple in site_dict:
                site_dict[varTuple] += 1
            else:
                site_dict[varTuple] = 1
        else:  # when read not passes the blat criteria
            if varTuple in discard_dict:
                discard_dict[varTuple] += 1
            else:
                discard_dict[varTuple] = 1

    for variant_line in open(variant_file):
        if len(variant_line) > 0 and variant_line[0] == "#":
            pass
        else:
            var_info = variant_line.split("\t")
            var_chr = var_info[0]
            var_pos = int(var_info[1])
            var_alt = var_info[3]
            var_ref = var_info[4]
            # chr, pos, ref, alt
            key = var_chr, var_pos, var_ref, var_alt
            number_blat_reads = site_dict[key] if key in site_dict else 0
            number_discard_reads = discard_dict[key] if key in discard_dict else 0
            if not number_blat_reads <= min_mismatch and not number_blat_reads <= number_discard_reads:
                print(variant_line.strip())


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(prog='remove_edge_mismatches')

    parser.add_argument('--psl',
                        type=str,
                        required=True,
                        help="A psl file.")

    parser.add_argument('--vcf',
                        type=str,
                        required=True,
                        help="A vcf file.")

    parser.add_argument('--min-base-qual',
                        type=int,
                        default=25,
                        help="")

    parser.add_argument('--min-mismatch',
                        type=int,
                        default=2,
                        help="")

    args = parser.parse_args()

    remove_duplicates(args.psl, args.vcf, args.min_base_qual, args.min_mismatch)
