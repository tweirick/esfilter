from pysam import Fastafile
import numpy as np
from random import shuffle
from collections import defaultdict, OrderedDict


class VariantSet(object):
    '''
    handles a vcfFile and stores the results internally as a Dictionary with Tuple of (chromosome,pos,ref,alt) as keys and a the VariantObject as value
    '''
    def __init__(self, vcfFile, logFile=None, textField=0):
        self.logFile = logFile
        self.textField = textField
        self.variantDict = self.parseVcf(vcfFile)


def calculate1dDistanceMatrix(lst, eps):
    '''
    creates a distance matrix for the given vector
    :param lst: vector of samples       
    :return: np.array(diffMatrix)
    '''
    if not isinstance(lst, (list, tuple, np.ndarray)):
        raise TypeError("Parameter has to be either a List or a Tuple found %s" % type(lst))
    if not all(isinstance(item, (int, float)) for item in lst):
        raise TypeError("List should only contain numbers")
    
    lst = np.asarray(lst)

    diff_matrix = []

    for l1 in lst:
        diff_list = []
        diff_list = abs(lst - l1)
        diff_list = np.where(diff_list <= eps)[0]
        diff_matrix.append(diff_list)

    return np.asarray(diff_matrix)


def get_labels(position_list, eps, min_samples):
        
    X = np.asarray(position_list)
    n = X.shape[0]  # get number of elements (not sure)
    index_order = range(n)

    shuffle(index_order)

    distanceMatrix = calculate1dDistanceMatrix(X, eps)
    
    # Calculate neighborhood for all samples. This leaves the original point
    # in, which needs to be considered later (i.e. point i is the
    # neighborhood of point i. While True, its useless information)
    
    # distanceMatrix = [np.where(x <= eps)[0] for x in distanceMatrix]
        
    # Initially, all samples are noise.
    labels = -np.ones(n, dtype=np.int)
    
    # A list of all core samples found.
    core_samples = []
    
    # label_num is the label given to the new cluster
    label_num = 0
    
    # Look at all samples and determine if they are core.
    # If they are then build a new cluster from them.
    for index in index_order:
        # Already classified
        if labels[index] != -1:
            continue
    
        # get neighbors from distanceMatrix or ballTree
        index_neighborhood = []
    
        index_neighborhood = distanceMatrix[index]

        # Too few samples to be core
        if len(index_neighborhood) < min_samples:
            continue
    
        core_samples.append(index)
        labels[index] = label_num
        # candidates for new core samples in the cluster.
        candidates = [index]
    
        while len(candidates) > 0:
            new_candidates = []
            # A candidate is a core point in the current cluster that has
            # not yet been used to expand the current cluster.
            for c in candidates:
                c_neighborhood = []
                    
                c_neighborhood = distanceMatrix[c]

                # indexes of candidate neigbours which do not belong to a cluster yet
                noise = np.where(labels[c_neighborhood] == -1)[0]
                noise = c_neighborhood[noise]
                labels[noise] = label_num
                for neighbor in noise:
                    n_neighborhood = []
                        
                    n_neighborhood = distanceMatrix[neighbor]
                        
                    # check if its a core point as well
                    if len(n_neighborhood) >= min_samples:
                        # is new core point
                        new_candidates.append(neighbor)
                        core_samples.append(neighbor)
            # Update candidates for next round of cluster expansion.
            candidates = new_candidates
        # Current cluster finished.
        # Next core point found will start a new cluster.
        label_num += 1
    # return core_samples, labels
    return labels


class Variant:
    """
    reflects a Variant
    """

    def __init__(self, chromosome, position, id, ref, alt, qual, filter, info):
        self.chromosome = chromosome
        self.position = position
        self.id = id
        self.ref = ref
        self.alt = alt
        self.qual = qual
        self.filter = filter
        self.attributes = info
        
    def __iter__(self):
        return self.var


class VarientSet: 
    def __init__(self, vcf_file_name): 
        self.variant_dict = OrderedDict()    
        for varient_line in open(vcf_file_name):
            if len(varient_line) > 0 and varient_line[0] == "#":
                pass
            else:    
                variant_chr, var_pos, var_id, var_ref, var_alt, var_qual, var_filter, var_info, var_format, var_A = varient_line.split()
                v = Variant(variant_chr, var_pos, var_id, var_ref, var_alt, var_qual, var_filter, var_info)
                self.variant_dict[(v.chromosome, v.position, v.ref, v.alt)] = v
    
    def getVariantListByChromosome(self):
        '''
        @return: variants as Dictionary with chromosome as key and a list of VariantObjects as values
        {"1":[VariantObject1,VariantObject2....],"2":[VariantObject1,VariantObject2....]}
        '''
        self.variantsByChromosome = defaultdict(list)
        for v in self.variant_dict.values():
            self.variantsByChromosome[v.chromosome].append(v)
        
        return self.variantsByChromosome



# variants, outFile, distance
def create_clusters(vcf_file_name, eps=50, min_samples=5):
    # Create a Pysam fasta file object
    
    islandCounter = 0
    eps = int(eps)
    min_samples = int(min_samples)
    position_list = []
    clusterDict = defaultdict(list)

    vcf_obj = VarientSet(vcf_file_name)

    vcf_obj.getVariantListByChromosome()

    for chr in vcf_obj.variantsByChromosome.keys():

        position_list = [int(v.position) for v in vcf_obj.variantsByChromosome[chr]]  # position of all variants from that chromosome
        
        labels = get_labels(position_list, eps, min_samples)

        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)

        if n_clusters_ > 0:
            # loop over labels and variants
            tmpDict = defaultdict(list)
            for var, label in zip(vcf_obj.variantsByChromosome[chr], labels):
                # clusterdict{1:[var1,var2],2:[var5,var8]}
                if label == -1:
                    continue        
                tmpDict[label].append(var)
            #set new label for clusterdict, to avoid overwriting
            for label in tmpDict.keys():
                clusterDict[islandCounter] = tmpDict.pop(label)
                islandCounter += 1

    print("\t".join(["#Chr", "Start", "Stop", "IslandID", "GeneID", 
                     "Gene Symbol", "Cluster Length", 
                     "Number of Editing_sites", "Editing_rate"]))
     
    for cluster in clusterDict.keys():
        end = max(int(v.position) for v in clusterDict[cluster])
        start = min(int(v.position) for v in clusterDict[cluster])
    
        length = end - start
        editingRate = float(len(clusterDict[cluster]))/float(length)
        geneIdSet = set()
        geneNameSet = set()
        for v in clusterDict[cluster]:
            pass
            #try: 
            #    gene = v.attributes['GI'][0][0]
            #    if type(gene) == Gene:
            #        geneIdSet.add(gene.geneId)
            #        geneNameSet |= set(gene.names)
            #        # geneList.append(v.attributes['GI'][0][0])
            #    else:
            #        geneIdSet.add("Intergenic")
            #        geneNameSet.add("Intergenic")
            #except KeyError:
            #    geneIdSet.add("N/A")  # when variant has no attribute GI
        
        print("\t".join([
                v.chromosome,           # "Chr",
                str(start),             # "Start",
                str(end),               # "Stop",
                "Island"+str(cluster),  # "Cluster Name",
               str(length), str(len(clusterDict[cluster])), '%1.2f'%float(editingRate)]))

if __name__ == "__main__":
    import argparse    

    parser = argparse.ArgumentParser(prog='remove_edge_mismatches')

    parser.add_argument('--vcf',
                        dest='vcf',
                        type=str,
                        required=True, 
                        help="A vcf file.")

    args = parser.parse_args()
        
    create_clusters(args.vcf)






