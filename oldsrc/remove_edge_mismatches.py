from pysam import Samfile
# variants.printVariantDict(self.rnaEdit.params.output+".noReadEdges.vcf")


def keep_snp(iter, var_pos_m1, min_base_quality, min_distance, var_alt): 

    # walks up the region which overlap this position
    for x in iter:
        if x.pos == var_pos_m1:
            # walk through the single reads

            for pileup_read in x.pileups: 

                if not pileup_read.is_del and not pileup_read.is_refskip:

                    distance = abs(pileup_read.alignment.alen - pileup_read.query_position) if pileup_read.alignment.is_reverse else pileup_read.query_position

                    if distance >= min_distance:

                        # check readBase and Base Quality
                        if (pileup_read.alignment.query_sequence[pileup_read.query_position] == var_alt and 
                        pileup_read.alignment.query_qualities[pileup_read.query_position] >= min_base_quality):
                            return True
    return False


def remove_edge_mismatches(vcf_file_name, bam_file_name, min_distance, min_base_quality):

    min_distance = int(min_distance)
    bam_file_obj = Samfile(bam_file_name, "rb")
    for variant_line in open(vcf_file_name):
        if len(variant_line) > 0 and variant_line[0] == "#":
            print(variant_line.strip())
        else:
            variant_chr, var_pos, var_id, var_ref, var_alt, var_qual, var_filter, var_info, var_format, var_A = variant_line.split()
            var_pos = int(var_pos)
            var_pos_m1 = var_pos - 1
            iter = bam_file_obj.pileup(variant_chr, var_pos_m1, var_pos)
            if keep_snp(iter, var_pos_m1, min_base_quality, min_distance, var_alt):
                print(variant_line.strip())
     
    bam_file_obj.close()


if __name__ == "__main__":
    import argparse    

    parser = argparse.ArgumentParser(prog='remove_edge_mismatches')

    parser.add_argument('--vcf',
                        dest='vcf',
                        type=str,
                        required=True, 
                        help="A vcf file.")

    parser.add_argument('--bam',
                        dest='bam',
                        type=str,
                        required=True, 
                        help="A bam file.")

    parser.add_argument('--min_distance',
                        type=int,
                        default=3,
                        help='')

    parser.add_argument('--min_base_quality',
                        type=int,
                        default=25,
                        help='')

    args = parser.parse_args()
    remove_edge_mismatches(args.vcf, args.bam, args.min_distance, args.min_base_quality)



