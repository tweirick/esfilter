import sys 


for line in open(sys.argv[1]): 
    if len(line) > 0 and line[0] != "#":
        split_line = line.strip().split()
        if split_line[2] == "gene": 
            gene_name = line.split('gene_name "')[-1].split('"')[0]
            gene_id = line.split('gene_id "')[-1].split('"')[0]
            print(
                "\t".join([
                    split_line[0],
                    split_line[3], 
                    split_line[4],
                    gene_name, 
                    gene_id
                ])
            )

