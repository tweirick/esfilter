#!/usr/bin/env python3
from pysam import Samfile
from pysam import Fastafile
from esfilterlib.parsers import yield_vcf_objects, BED, generate_fasta_from_vcf_and_bam
from esfilterlib.filters import *
import sys
from itertools import chain, combinations

run_doc_tests = False
unannotated_id_symbol = "."
types_of_editing_mismatches_set = {("A", "G"), ("T", "C")}
in_set_symbol = "O"
not_in_set_symbol = "-"
delimiter = "\t"


def get_power_set_of_frozen_sets(iterable):
    """ Calculate the powerset given an iterable data type.
    >>> get_powerset_of_frozensets([1, 2])
    {frozenset({1, 2}), frozenset({2}), frozenset(), frozenset({1})}

    :param iterable:
    :return: out_set - a set of frozensets representing the powerset of the input iterable.
    """
    xs = list(iterable)
    # note we return an iterator rather than a list
    out_set = set()
    for member in chain.from_iterable(combinations(xs, n) for n in range(len(xs) + 1)):
        out_set.add(frozenset(member))
    return out_set


if __name__ == "__main__":

    import argparse

    parser = argparse.ArgumentParser()

    # Sample files
    parser.add_argument('--vcf',
                        dest='vcf',
                        type=str,
                        required=True,
                        help="A vcf file non-editing sites and potential A-to-I false positives will be removed.")

    parser.add_argument('--bam',
                        dest='bam',
                        type=str,
                        required=True,
                        help="The BAM file used for genotype calling.")

    # Reference files
    parser.add_argument('--splice-junctions',
                        type=str,
                        default=False,
                        help="A bed file containing splice junctions.")

    parser.add_argument('--repeat-regions',
                        type=str,
                        default=False,
                        help="A bed file containing locations of repetitive elements.")

    parser.add_argument('--reference-genome',
                        dest='reference_genome',
                        type=str,
                        required=True,
                        help="A reference genome fasta file.")

    # Output files
    parser.add_argument('--filter-stats',
                        type=str,
                        default=False,
                        help="")

    parser.add_argument('--repeat-regions-out-vcf',
                        type=str,
                        default=False,
                        help="")

    parser.add_argument('--non-repeat-regions-out-vcf',
                        type=str,
                        default=False,
                        help="")

    parser.add_argument('--in-repeat-regions-out-fasta',
                        type=str,
                        default=False,
                        help="")

    # Parameters
    parser.add_argument('--min-edge-distance',
                        type=int,
                        default=3,
                        help='')

    parser.add_argument('--homopolymer-distance',
                        type=int,
                        default=4,
                        help='The distance to check for homopolymers.')

    parser.add_argument('--min-base-quality',
                        type=int,
                        default=25,
                        help='')

    parser.add_argument('--min-mismatch',
                        type=int,
                        default=2,
                        help="")

    test_all_help_text = "Test each base for all filtering criteria, " \
                         "otherwise bases are only tested until first failed filter."

    parser.add_argument('--test-all', action='store_true', help=test_all_help_text)

    args = parser.parse_args()

    filtered_reasons = {}
    total_passed = 0
    total_lines = 0
    filters = {}
    power_set_counting_dict = {}
    power_set_set = set()

    calculate_filter_stats = args.filter_stats

    # Open out file objects
    repeat_vcf_file_obj = open(args.repeat_regions_out_vcf, 'w')
    non_repeat_vcf_file_obj = open(args.non_repeat_regions_out_vcf, 'w')
    non_repeat_fasta_file_obj = open(args.in_repeat_regions_out_fasta, 'w')

    # Set variables
    minimum_base_quality = int(args.min_base_quality)
    minimum_edge_distance = int(args.min_edge_distance)
    minimum_mismatches = int(args.min_mismatch)
    homo_polymer_distance = int(args.homopolymer_distance)

    test_all = args.test_all

    # Reference files
    bam_obj = Samfile(args.bam, "rb")
    # print("BAM loaded.", file=sys.stderr)

    fasta_obj = Fastafile(args.reference_genome)
    # print("FASTA loaded.", file=sys.stderr)

    sj_bed_obj = BED(args.splice_junctions)
    # print("Splice junctions loaded.", file=sys.stderr)

    repeat_regions_bed_obj = BED(args.repeat_regions)
    # print("Repeat regions loaded.", file=sys.stderr)

    tmp_filtered_reasons = {}
    mbq = minimum_base_quality
    med = minimum_edge_distance
    hd = homo_polymer_distance
    for vcf in yield_vcf_objects(args.vcf):

        chromosome = vcf.chromosome
        pos = vcf.position
        rb = vcf.reference_base
        ab = vcf.alternative_base
        site_filter_results = {}

        is_in_repeat = is_in_region(repeat_regions_bed_obj, chromosome, pos)

        has_not_failed_filter = True
        if has_not_failed_filter or test_all:
            is_snp_result = is_snp(vcf.id)
            site_filter_results["SNP"] = is_snp_result
            # If SNP then we have failed the filter, else
            has_not_failed_filter = is_snp_result is False

        if has_not_failed_filter or test_all:
            is_nonediting_mismatch_result = is_nonediting_mismatch(rb, ab)
            site_filter_results["NOT_EDITING"] = is_nonediting_mismatch_result

        if has_not_failed_filter or test_all:
            is_edge_mismatch_result = is_edge_mismatch(bam_obj, chromosome, pos, ab, mbq, med)
            site_filter_results["EDGE_MISMATCH"] = is_edge_mismatch_result
            has_not_failed_filter = is_edge_mismatch_result is False

        if not is_in_repeat:

            if has_not_failed_filter or test_all:
                is_in_splice_junction_result = is_in_region(sj_bed_obj, chromosome, pos)
                site_filter_results["SPLICE_JUNCTION"] = is_in_splice_junction_result
                has_not_failed_filter = is_in_splice_junction_result is False

            if has_not_failed_filter or test_all:
                is_homopolymer_result = is_homopolymer(chromosome, pos, rb, fasta_obj, hd)
                site_filter_results["HOMOPOLYMER"] = is_homopolymer_result
                has_not_failed_filter = is_homopolymer_result is False

        failed_filters = []
        for key, val in site_filter_results.items():
            if val:
                failed_filters.append(key)

        # We don't want to remove things in repeat regions, only measure them.
        # If there are no failed filters then output the line.

        if len(failed_filters) == 0:

            total_passed += 1
            #  site_filter_results["IN_REPEAT"] = is_in_region(
            #  repeat_regions_bed_obj, chromosome, pos)

            site_filter_results["IN_REPEAT"] = is_in_repeat
            if site_filter_results["IN_REPEAT"]:
                failed_filters.append("IN_REPEAT")
                # Write to repeats to file
                repeat_vcf_file_obj.write(vcf.original_record)
            else:
                # Write to non-repeats file
                # Generate output fasta
                out_text = generate_fasta_from_vcf_and_bam(
                    vcf.chromosome, vcf.position, vcf.reference_base, vcf.alternative_base,
                    minimum_base_quality, minimum_mismatches, bam_obj)

                non_repeat_fasta_file_obj.write(out_text)

                non_repeat_vcf_file_obj.write(vcf.original_record)

    repeat_vcf_file_obj.close()
    non_repeat_vcf_file_obj.close()
    # non_repeat_fasta_file_obj.close()
'''
        if calculate_filter_stats and test_all:
            total_lines += 1

            if power_set_counting_dict == {}:
                filters = sorted(site_filter_results)
                power_set_set = get_power_set_of_frozen_sets(site_filter_results)
                power_set_counting_dict = {member: 0 for member in power_set_set}

            failed_filters_frozen_set = {frozenset(failed_filters)}
            for shared_members in failed_filters_frozen_set & power_set_set:
                power_set_counting_dict[shared_members] += 1
                # Will have at least one False value
                # Make set of false values

            failed_filters = []
            for key, val in site_filter_results.items():
                if val:
                    failed_filters.append(key)

    # Handle filter output
    if calculate_filter_stats:

        with open(args.filter_stats, 'w') as stat_file_obj:
            # Print titles
            print(delimiter.join(list(filters) + ["Cardinality"]), file=stat_file_obj)

            for sorted_member in sorted(power_set_set):

                if len(sorted_member) == 0:
                    print("Passed", end=delimiter, file=stat_file_obj)
                else:
                    print("&".join(list(sorted_member)), end=delimiter, file=stat_file_obj)

                for filter_name in filters:
                    print_symbol = in_set_symbol if filter_name in sorted_member else not_in_set_symbol
                    print(print_symbol, end=delimiter, file=stat_file_obj)

                print(power_set_counting_dict[sorted_member], file=stat_file_obj)

            # non_filtered = [not_in_set_symbol for i in range(len(filters))]
            non_filtered = ["U" for i in range(len(filters))]
            print(delimiter.join(["Total"] + non_filtered + [str(total_lines)]), file=stat_file_obj)
'''

