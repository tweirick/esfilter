from intervaltree import IntervalTree, Interval
from collections import namedtuple

vcf_record = namedtuple('VCFRecord', [
    "chromosome", "position", "id", "reference_base", "alternative_base", "original_record"
])


class BED:

    def __init__(self, bed_file_path=None):
        """

        :param bed_file_path:
        """
        self.interval_tree_bed_dict = {}
        self.interval_value = True
        if bed_file_path:
            self.add_bed_records_from_file(bed_file_path)

    def add_bed_records_from_file(self, bed_file_path):
        cnt = 0
        with open(bed_file_path) as bed_file_obj:
            for bed_row in bed_file_obj:
                cnt += 1
                chromosome, start, stop = bed_row.split()[:3]
                self.add_interval(chromosome, int(start), int(stop))

    def add_interval(self, chromosome, start, stop):
        """

        :param chromosome:
        :param start:
        :param stop:
        :return:
        """
        chromosome, start, stop = str(chromosome), int(start), int(stop)
        try:
            self.interval_tree_bed_dict[chromosome][start:stop] = self.interval_value
        except KeyError:
            self.interval_tree_bed_dict[chromosome] = IntervalTree()
            self.interval_tree_bed_dict[chromosome][start:stop] = self.interval_value

    def is_base_within_region(self, chromosome, position):
        """

        :param chromosome:
        :param position:
        :return:
        """

        chromosome, position = str(chromosome), int(position)
        zero_based_start = position - 1
        try:
            if self.interval_tree_bed_dict[chromosome].overlaps(zero_based_start, position):
                return True
        except KeyError:
            pass

        return False


def vcf_to_namedtuple(line):
    """ Convert a row in a vcf table into a named tuple.

    >>> vcf_line = "1\t237291723\trs7538075\tC\tA\t7.40\t.\tDels=0.00;FS=0.000;\tGT:AD:DP:GQ:PL\t1/1:0,1:1:3:33,3,0"
    >>> vcf_to_namedtuple(vcf_line)
    VCFRecord(chromosome='1', position=237291723, id='rs7538075', reference_base='C', alternative_base='A', original_record="1\t237291723\trs7538075\tC\tA\t7.40\t.\tDels=0.00;FS=0.000;\tGT:AD:DP:GQ:PL\t1/1:0,1:1:3:33,3,0")

    :param line:
    :return:
    """
    sl = line.split()

    return vcf_record(
        sl[0],       # chromosome
        int(sl[1]),  # position
        sl[2],       # tmp_id
        sl[3],       # reference_base
        sl[4],       # alternative_base
        line
    )


def yield_vcf_objects(vcf_file_path):
    """

    :param vcf_file_path:
    :return:
    """
    with open(vcf_file_path) as file_obj:
        line = None
        while True:
            line = file_obj.readline()
            if not line.startswith('#'):
                break

        while line:
            yield vcf_to_namedtuple(line)
            # Get new line last
            line = file_obj.readline()


def generate_fasta_from_vcf_and_bam(chromosome, position, reference_base, alternative_base,
                                    min_base_quality, min_mismatch, bam_obj):
    """
    :param chromosome:
    :param position:
    :param reference_base:
    :param alternative_base:
    :param min_base_quality:
    :param min_mismatch:
    :param bam_obj:
    :return:
    """
    position = int(position)
    position_minus_one = position - 1

    alignments = []
    for x in bam_obj.pileup(chromosome, position_minus_one, position):
        if x.pos == position_minus_one:
            # loop over reads of that position
            for pileup_read in x.pileups:
                if not pileup_read.is_del and not pileup_read.is_refskip:
                    loc_base = pileup_read.alignment.query_sequence[pileup_read.query_position]
                    loc_qual = pileup_read.alignment.query_qualities[pileup_read.query_position]
                    if loc_base == alternative_base and loc_qual >= min_base_quality:
                        alignments.append(pileup_read.alignment.seq)

    out_text_list = []
    if len(alignments) >= min_mismatch:
        mismatch_read_count = 0
        for sequence in alignments:
            out_text_list.append(
                ">%s-%s-%s-%s-%s\n%s\n" % (
                    chromosome, position,
                    reference_base, alternative_base,
                    mismatch_read_count, sequence
                )
            )
            mismatch_read_count += 1

    return "".join(out_text_list)


if __name__ == "__main__":
    import doctest
    doctest.testmod()
