types_of_editing_mismatches_set = {("A", "G"), ("T", "C")}


def is_snp(base_id, unannotated_id_symbol="."):
    """ Check to see if an entry in a VCF file has a SNP ID.

    >>> is_snp("rs1800351")
    True

    >>> is_snp(".")
    False

    :param base_id:
    :param unannotated_id_symbol:
    :return:
    """
    if base_id == unannotated_id_symbol:
        return False
    return True


def is_nonediting_mismatch(reference_base, alternative_base, allowed_mismatches=types_of_editing_mismatches_set):
    """ Return True if a mismatch is a potential editing site, else return False.

    >>> is_allowed_mismatch("A", "G")
    True

    >>> is_allowed_mismatch("G", "C")
    False

    :return:
    """
    if (reference_base, alternative_base) in allowed_mismatches:
        return False
    return True


def keep_snp(iterator, var_pos_m1, min_base_quality, min_distance, var_alt):
    """

    :param iterator:
    :param var_pos_m1:
    :param min_base_quality:
    :param min_distance:
    :param var_alt:
    :return:
    """
    # Walks up the region which overlap this position
    alignments = []
    for x in iterator:
        if x.pos == var_pos_m1:
            # walk through the single reads

            for pileup_read in x.pileups:

                if not pileup_read.is_del and not pileup_read.is_refskip:

                    if pileup_read.alignment.is_reverse:
                        distance = abs(pileup_read.alignment.alen - pileup_read.query_position)
                    else:
                        distance = pileup_read.query_position

                    if distance >= min_distance:
                        read_base = pileup_read.alignment.query_sequence[pileup_read.query_position]
                        read_quality = pileup_read.alignment.query_qualities[pileup_read.query_position]

                        is_base_above_minimum_quality = read_quality >= min_base_quality
                        is_alternative_base = read_base == var_alt

                        # Check read base and base quality
                        if is_alternative_base and is_base_above_minimum_quality:
                            alignments.append(pileup_read.alignment.seq)
                            return True
    return False


def is_edge_mismatch(bam_obj, chromosome, position, alternative_base,
                     minimum_base_quality, minimum_edge_distance):
    """ Return True is a base is an edge mismatch.

    @TODO: Write tests.

    :param bam_obj:
    :param chromosome:
    :param position:
    :param minimum_base_quality:
    :param minimum_edge_distance:
    :param alternative_base:
    :return:
    """
    position_minus_1 = position - 1
    pileup_obj = bam_obj.pileup(chromosome, position_minus_1, position)
    if keep_snp(pileup_obj, position_minus_1, minimum_base_quality, minimum_edge_distance, alternative_base):
        return False
    return True


# =====================================================
#              Check for splice junctions
# =====================================================
def is_in_region(bed_obj, chromosome, position):
    """

    :param bed_obj:
    :param chromosome:
    :param position:
    :return:
    """
    if bed_obj.is_base_within_region(chromosome, position):
        return True
    return False


def is_homopolymer(chromosome, position, reference_base, fasta_obj, homopolymer_distance):
    """

    :param chromosome:
    :param position:
    :param reference_base:
    :param homopolymer_distance:
    :return:
    """
    homopolymer_start_position = position - homopolymer_distance if position >= homopolymer_distance else 0
    homopolymer_end_position = position + homopolymer_distance
    pattern = reference_base * homopolymer_distance

    # Fasta sequences are zero based
    fasta_txt = fasta_obj.fetch(chromosome, homopolymer_start_position, homopolymer_end_position)

    if pattern in fasta_txt:
        return True
    return False

