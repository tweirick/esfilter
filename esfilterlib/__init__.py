
"""
def check_site(chromosome, position, base_id, reference_base, alternative_base,
               minimum_base_quality,
               minimum_edge_distance,
               homopolymer_distance,
               bam_obj,
               fasta_obj,
               sj_bed_obj
               ):

    filtered_reasons = {}
    #    "IS_SNP": True,
    #    "IS_NOT_EDITING_MISMATCH": True,
    #    # "IS_MISMATCH": True,
    #    # "IS_EDGE_MISMATCH": True,
    #    # "IS_IN_SPLICE_JUNCTION": True,
    #    # "IS_HOMOPOLYMER": True
    # }

    position_minus_1 = position - 1
    # =====================================================
    #              Check for annotated SNPs
    # =====================================================
    # Known SNPs will be annotated in the Genotype Calling step
    # Therefore we can simply exclude anything with a SNP ID
    filtered_reasons["IS_SNP"] = True
    if base_id == unannotated_id_symbol:
        filtered_reasons["IS_SNP"] = False

    # =====================================================
    #              Check for non-editing mismatches
    # =====================================================
    filtered_reasons["IS_NOT_EDITING_MISMATCH"] = True
    if (reference_base, alternative_base) in types_of_editing_mismatches_set:
        filtered_reasons["IS_NOT_EDITING_MISMATCH"] = False

    # =====================================================
    #              Check for edge mismatches
    # =====================================================
    filtered_reasons["EDGE_MISMATCH"] = True
    pileup_obj = bam_obj.pileup(chromosome, position_minus_1, position)
    if keep_snp(pileup_obj, position_minus_1, minimum_base_quality, minimum_edge_distance, alternative_base):
        filtered_reasons["EDGE_MISMATCH"] = False

    # =====================================================
    #              Check for splice junctions
    # =====================================================
    #     def is_base_within_region(self, chromosome, position):
    filtered_reasons["IS_IN_SPLICE_JUNCTION"] = True
    if sj_bed_obj.is_base_within_region(chromosome, position):
        filtered_reasons["IS_IN_SPLICE_JUNCTION"] = False

    # =====================================================
    #              Check for homopolymers
    # =====================================================
    homopolymer_start_position = position - homopolymer_distance if position >= homopolymer_distance else 0
    homopolymer_end_position = position + homopolymer_distance
    pattern = reference_base * homopolymer_distance

    # Fasta sequences are zero based
    if pattern not in fasta_obj.fetch(chromosome, homopolymer_start_position, homopolymer_end_position):
        filtered_reasons["IS_HOMOPOLYMER"] = False

    # return filtered_reasons
"""

